import Noty from 'noty'
import 'noty/lib/noty.css'
import 'noty/lib/themes/relax.css'
import './notification.scss'

// ----------------------------------------------------

export default class Notification extends Noty {}
